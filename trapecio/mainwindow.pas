unit mainwindow;

{$mode objfpc}{$H+}

interface

uses
   Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
   TrapezoidalMethod;

type

   { TForm1 }

   TForm1 = class(TForm)
      btnEval: TButton;
      edtHigh: TEdit;
      edtLow: TEdit;
      edtNumOfIntervals: TEdit;
      edtFunction: TEdit;
      lblHigh: TLabel;
      lblLow: TLabel;
      lblNumOfIntervals: TLabel;
      lblFunction: TLabel;
      procedure btnEvalClick(Sender: TObject);
      procedure FormCreate(Sender: TObject);
      procedure FormDestroy(Sender: TObject);
   private
      method : TTrapezoidalMethod;
   public
      { public declarations }
   end;

var
   Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.btnEvalClick(Sender: TObject);
var
   high, low : real;
   intervals : integer;
begin
   if (edtFunction.GetTextLen = 0) then
   begin
      ShowMessage('Ingrese una función.');
      exit;
   end;
   if (edtLow.GetTextLen = 0) then
   begin
      ShowMessage('Ingrese el límite inferior.');
      exit;
   end;
   if (edtHigh.GetTextLen = 0) then
   begin
      ShowMessage('Ingrese el límite superior.');
      exit;
   end;
   if (edtNumOfIntervals.GetTextLen = 0) then
   begin
      ShowMessage('Ingrese la cantidad de intervalos.');
      exit;
   end;

   high := strToFloat(edtHigh.text);
   low := strToFloat(edtLow.text);
   intervals := StrToInt(edtNumOfIntervals.text);

   method.setFunction(edtFunction.text);
   method.setLowHigh(low, high);
   method.setNumOfIntervals(intervals);

   ShowMessage(floatToStr(method.eval()));

end;

procedure TForm1.FormCreate(Sender: TObject);
begin
   method := TTrapezoidalMethod.create();
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
   method.destroy();
end;

end.
